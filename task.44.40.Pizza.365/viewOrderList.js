"use strict";
$().ready(function () {
  // Comment thêm dòng này để thử cho gitlab
  // Git branch checkout - edit something new
  onPageLoading();

  $("#order-table").on("click", ".btn-detail", toViewOrderDetailForm);

  $("#btn-confirm").on("click", onBtnConfirmClick);

  $("#btn-cancel").on("click", onBtnCancelClick);

  $("#order-modal").on("hidden.bs.modal", function () {
    resetFormToNormal();
  });
});

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";

var gFORM_MODE_NORMAL = "Normal";
var gFORM_MODE_INSERT = "Insert";
var gFORM_MODE_UPDATE = "Update";
var gFORM_MODE_DELETE = "Delete";

// biến toàn cục cho trạng thái của form: mặc định ban đầu là trạng thái Normal
var gFormMode = gFORM_MODE_NORMAL;

var gOrderResObj = {
  orders: [],
};

var gOrderIdObj = {
  id: "",
  orderId: "",
};

const gCOLUMN_ORDER_ID = 0;
const gCOLUMN_COMBO_SIZE = 1;
const gCOLUMN_PIZZA_TYPE = 2;
const gCOLUMN_DRINK = 3;
const gCOLUMN_AMOUNT = 4;
const gCOLUMN_NAME = 5;
const gCOLUMN_PHONE = 6;
const gCOLUMN_STATUS = 7;
const gCOLUMN_DETAIL = 8;

var gORDER_COL = [
  "orderId",
  "kichCo",
  "loaiPizza",
  "idLoaiNuocUong",
  "thanhTien",
  "hoTen",
  "soDienThoai",
  "trangThai",
  "detail",
];

var gOderTable = $("#order-table").DataTable({
  paging: false,
  info: false,
  // Khai báo các cột của datatable
  columns: [
    { data: gORDER_COL[gCOLUMN_ORDER_ID] },
    { data: gORDER_COL[gCOLUMN_COMBO_SIZE] },
    { data: gORDER_COL[gCOLUMN_PIZZA_TYPE] },
    { data: gORDER_COL[gCOLUMN_DRINK] },
    { data: gORDER_COL[gCOLUMN_AMOUNT] },
    { data: gORDER_COL[gCOLUMN_NAME] },
    { data: gORDER_COL[gCOLUMN_PHONE] },
    { data: gORDER_COL[gCOLUMN_STATUS] },
    { data: gORDER_COL[gCOLUMN_DETAIL] },
  ],
  // Ghi đè nội dung của cột action, chuyển thành button chi tiết
  columnDefs: [
    {
      targets: gCOLUMN_DETAIL,
      defaultContent: `<button class='btn btn-info btn-detail'>Chi tiết</button>`,
    },
  ],
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện loading
function onPageLoading() {
  // console.log("hien thi du lieu");
  callAPIToLoadOrderListObject();
}

function callAPIToLoadOrderListObject() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    dataType: "json",
    success: function (resOrder) {
      // console.log(resOrder);
      gOrderResObj.orders = resOrder;
      gOrderResObj.orders = gOrderResObj.orders.slice(0, 100);
      tableLoading(gOrderResObj.orders);
    },
    error: function (ajaxContext) {
      console.log(ajaxContext.statusText);
    },
  });
}

function tableLoading(paramOrders) {
  gOderTable.clear();
  gOderTable.rows.add(paramOrders);
  gOderTable.draw();
}

function toViewOrderDetailForm() {
  "use strict";
  // console.log("Đây là nút chi tiết");
  var vRowSelected = $(this).parents("tr");
  // Lấy data of row
  var vDataRow = gOderTable.row(vRowSelected);
  var vRowOrderData = vDataRow.data();
  // console.log(vRowOrderData);
  var { id, orderId } = vRowOrderData;
  gOrderIdObj = { id, orderId };
  callAPIToLoadOrderByOrderId(orderId);
  // console.log(gOrderIdObj);
  $("#order-modal").modal("show");
}

function callAPIToLoadOrderByOrderId(paramOrderId) {
  $.ajax({
    url: gBASE_URL + "/" + paramOrderId,
    type: "GET",
    dataType: "json",
    success: function (resOrder) {
      // console.log(resOrder);
      orderModalLoading(resOrder);
    },
    error: function (ajaxContext) {
      console.log(ajaxContext.statusText);
    },
  });
}

function orderModalLoading(paramOrder) {
  // Truy xuất các input element bằng id
  var vInputOrderIdElement = $("#order-id");
  var vInputComboSizeElement = $("#combo-size");
  var vInputPizzaDiameterElement = $("#pizza-diameter");
  var vInputRibPieceElement = $("#rib-piece");
  var vInputSaladElement = $("#salad");
  var vInputPizzaTypeElement = $("#pizza-type");

  var vInputVoucherIdElement = $("#voucher-id");
  var vInputAmountElement = $("#amount");
  var vInputDiscountElement = $("#discount");

  var vInputSelectedDrinkElement = $("#selected-drink");
  var vInputNumberDrinkElement = $("#number-drink");

  var vInputFullNameElement = $("#full-name");
  var vInputEmailElement = $("#email");
  var vInputPhoneNumberElement = $("#phone-number");
  var vInputAddressElement = $("#address");
  var vInputMessageElement = $("#message");
  var vInputOrderStatusElement = $("#order-status");
  var vInputCreatedDateElement = $("#created-date");
  var vInputUpdatedDateElement = $("#updated-date");

  const {
    orderId,
    kichCo,
    duongKinh,
    suon,
    salad,
    loaiPizza,
    idVourcher,
    thanhTien,
    giamGia,
    idLoaiNuocUong,
    soLuongNuoc,
    hoTen,
    email,
    soDienThoai,
    diaChi,
    loiNhan,
    trangThai,
    ngayTao,
    ngayCapNhat,
  } = paramOrder;

  // Gán value cho các input element với Data lấy được từ Order Object
  vInputOrderIdElement.val(orderId);
  vInputComboSizeElement.val(kichCo);
  vInputPizzaDiameterElement.val(duongKinh);
  vInputRibPieceElement.val(suon);
  vInputSaladElement.val(salad);
  vInputPizzaTypeElement.val(loaiPizza);

  vInputVoucherIdElement.val(idVourcher);
  vInputAmountElement.val(thanhTien);
  vInputDiscountElement.val(giamGia);

  vInputSelectedDrinkElement.val(idLoaiNuocUong);
  vInputNumberDrinkElement.val(soLuongNuoc);

  vInputFullNameElement.val(hoTen);
  vInputEmailElement.val(email);
  vInputPhoneNumberElement.val(soDienThoai);
  vInputAddressElement.val(diaChi);
  vInputMessageElement.val(loiNhan);
  vInputOrderStatusElement.val(trangThai);

  // Chuyển đổi data nhận được thành định dạng Date (ngày tháng năm)
  var vCreatedDate = new Date(ngayTao);
  var vUpdatedDate = new Date(ngayCapNhat);
  // console.log(vCreatedDate.toDateString());
  vInputCreatedDateElement.val(vCreatedDate.toDateString());
  vInputUpdatedDateElement.val(vUpdatedDate.toDateString());
}

// Ấn nút confirm chuyển trạng thái của Order và update thông tin bằng API
function onBtnConfirmClick() {
  "use strict";
  // console.log("Confirm click - Id", gOrderIdObj.id);
  // console.log("Confirm click - OrderId", gOrderIdObj.orderId);
  var vOrderStatus = "confirmed";
  updateOrderStatus(vOrderStatus);

  alert(
    `Hãy copy Order Id & reload trang để kiểm tra tình trạng đơn hàng: ${gOrderIdObj.orderId}`
  );
  $("#order-modal").modal("hide");

  resetFormToNormal();
  // location.reload();
}

// Ấn nút cancel chuyển trạng thái của Order và update thông tin bằng API
function onBtnCancelClick() {
  "use strict";
  console.log("Cancel click - Id", gOrderIdObj.id);
  var vOrderStatus = "cancel";
  updateOrderStatus(vOrderStatus);
  alert(
    `Hãy copy Order Id & reload trang để kiểm tra tình trạng đơn hàng: ${gOrderIdObj.orderId}`
  );
  $("#order-modal").modal("hide");

  resetFormToNormal();
  // location.reload();
}

// Nhận status khi nhấn các button Confirm hoặc Cancel và redirect về View Order List
function updateOrderStatus(paramOrderStatus) {
  "use strict";
  var vId = gOrderIdObj.id;
  var vConfirmRequest = {
    trangThai: paramOrderStatus, //3 trang thai open, confirmed, cancel
  };

  $.ajax({
    url: gBASE_URL + "/" + vId,
    // async: false,
    type: "PUT",
    contentType: "application/json;charset=UTF-8",
    data: JSON.stringify(vConfirmRequest),
    success: function (responseOrder) {
      console.log(responseOrder);
    },
    error: function (ajaxContext) {
      alert(ajaxContext.responseText);
    },
  });
}

function resetFormToNormal() {
  // đổi trạng thái về normal
  gFormMode = gFORM_MODE_NORMAL;
  $("#div-form-mod").html(gFormMode);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
